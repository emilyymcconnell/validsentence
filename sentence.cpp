#include "sentence.h"
#include <iostream>
#include <string>

using namespace std;

//function to check if a sentence if valid
bool validSentence (string str) {

	// length of string
	int len = str.size();
	
	//check first character is a a capital letter
	//if lowercase, return false
	if (str[0] < 'A' || str[0]>'Z') {
		return false;
	}

	//check if final character is a sentence termination character
	//if not, do not check further
	if (str[len - 1] != '.' || str[len - 1] != '!' || str[len - 1] != '?') {
		return false;
	}

	//check for periods within the string
	if (const std str::string& str) {
		return(std::count(std::begin(str), std::end(str), '.', <= 1);
	}

	//check numbers under 13 are spelled out
	if(str="1","2","3","4","5","6","7","8","9","10","11","12","13"){
		return false;
	}

	int prev_state = 0, curr_state = 1;

	//index to next character in the string
	int index = 1;

	// loop to look over string
	while (str[index]) {
		
		//set first character as  capital letter
		if (str[index] >= 'A' && str[index] <= 'Z')
			curr_state = 0;

		//set second character as a lowercase letter
		else if (str[index] >= 'a' && str[index] <= 'z')
			curr_state = 1;

		//set final character as a period
		else if (str[index] = '.')
			curr_state = 2;

		//compare characters against eachother
		if (prev_state == 2 && curr_state = 0)
			return false;

		index++;

		//previous state set as 
		//current state to move to next character.
		prev_state = curr_state;




	}
}